/**
 * @author Nil Panchal Mex IT Jun 7, 2022 9:34:16 PM SMSController.java com.springdemo.controller
 *         Sms
 */
package com.springdemo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.springdemo.constants.Constants;
import com.springdemo.payload.request.SMSRequestPayload;
import com.springdemo.payload.response.ApiResponse;
import com.springdemo.service.SMSService;

/**
 * @author Nil Panchal Mex IT Jun 7, 2022 9:34:16 PM
 */
@RestController
public class SMSController {

  private static final Logger log = LoggerFactory.getLogger(SMSService.class);
  @Autowired
  SMSService apiService;

  @PostMapping(Constants.SEND_SMS)
  public ResponseEntity<ApiResponse> sendSMS(@RequestBody SMSRequestPayload request) {
    log.info("sendSMS ->");
    ApiResponse response = apiService.sendSmsMessage(request);
    return ResponseEntity.status(response.getCode()).body(response);
  }
}
