/**
 * @author Nil Panchal Mex IT Jun 7, 2022 10:14:09 PM SMSComponent.java com.springdemo.component Sms
 */
package com.springdemo.component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.springdemo.constants.Constants;

/**
 * @author Nil Panchal Mex IT Jun 7, 2022 10:14:09 PM
 */
@Component
public class SMSComponent {

  private static final Logger log = LoggerFactory.getLogger(SMSComponent.class);

  /**
   * Function to split the text into chuncks
   * 
   * @param text
   * @param suffix
   * @param chunkSize
   * @param stringSplittSize
   * @param limit
   * @return
   */
  public List<String> splittSMSText(String text, String suffix, double chunkSize,
      int stringSplittSize, int limit) {
    log.info("splittSMSText -> ");
    AtomicInteger index = new AtomicInteger(1);
    return IntStream.iterate(0, i -> i + stringSplittSize).limit(limit).mapToObj(i -> {
      String subText = text.substring(i, Math.min(text.length(), i + stringSplittSize))
          + suffix.replace(Constants.PLACEHOLDER_X, String.valueOf(index.getAndAdd(1)))
              .replace(Constants.PLACEHOLDER_Y, String.valueOf(limit));
      return subText;
    }).collect(Collectors.toList());
  }
}
