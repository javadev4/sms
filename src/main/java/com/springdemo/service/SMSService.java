/**
 * @author Nil Panchal Mex IT Jun 7, 2022 9:44:35 PM SMSService.java com.springdemo.service Sms
 */
package com.springdemo.service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.springdemo.component.SMSComponent;
import com.springdemo.payload.request.SMSRequestPayload;
import com.springdemo.payload.response.ApiResponse;
import com.springdemo.constants.Constants;

/**
 * @author Nil Panchal Mex IT Jun 7, 2022 9:44:35 PM
 */
@Service
public class SMSService {

  private static final Logger log = LoggerFactory.getLogger(SMSService.class);


  @Autowired
  SMSComponent apiComponent;

  /**
   * Function to send sms
   * 
   * @param request
   * @return
   */
  public ApiResponse sendSmsMessage(SMSRequestPayload request) {
    log.info("sendSmsMessage -> ");
    double maxChuckSize = (request.getChunkSize() - 1);
    int stringSplittSize = (int) (maxChuckSize - Constants.SUFFIX.length());
    int limit = (int) Math.ceil(request.getText().length() / (double) stringSplittSize);
    List<String> textArray = apiComponent.splittSMSText(request.getText(), Constants.SUFFIX,
        request.getChunkSize(), stringSplittSize, limit);
    textArray.stream()
        .forEachOrdered(data -> deliverMessageViaCarrier(data, request.getTo(), request.getFrom()));
    return new ApiResponse(200, true, "Success", textArray, limit);
  }

  /**
   * Function to deliver message
   * 
   * @param text
   * @param to
   * @param from
   */
  private void deliverMessageViaCarrier(String text, String to, String from) {
    log.info("[" + from + " -> " + to + "]" + " : " + text);
  }
}
