/**
 * @author Nil Panchal Mex IT Jun 8, 2022 11:05:10 PM Constants.java com.springdemo.constants Sms
 */
package com.springdemo.constants;

/**
 * @author Nil Panchal Mex IT Jun 8, 2022 11:05:10 PM
 */
public class Constants {
  /**
   * Application's URLs
   */
  public static final String SEND_SMS = "/sendsms";

  /**
   * Application constants
   */
  public static final String PLACEHOLDER_X = "X";
  public static final String PLACEHOLDER_Y = "Y";
  public static final String SUFFIX = " - Part " + PLACEHOLDER_X + " of " + PLACEHOLDER_Y;
  
  
}
