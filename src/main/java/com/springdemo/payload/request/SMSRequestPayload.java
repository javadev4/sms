/**
 * @author Nil Panchal Mex IT Jun 7, 2022 9:35:23 PM SMSRequestPayload.java com.springdemo.controller
 *         Sms
 */
package com.springdemo.payload.request;

/**
 * @author Nil Panchal Mex IT Jun 7, 2022 9:35:23 PM
 */
public class SMSRequestPayload {
  private String text;
  private String from;
  private String to;
  private double chunkSize;

  /**
   * @author Nil Panchal Jun 7, 2022 9:35:38 PM
   */
  public SMSRequestPayload() {
    super();
  }

  
  /**
   * @author Nil Panchal
   * Jun 8, 2022
   * 10:32:22 PM
   * @param text
   * @param from
   * @param to
   * @param chunkSize
   */
  public SMSRequestPayload(String text, String from, String to, double chunkSize) {
    this.text = text;
    this.from = from;
    this.to = to;
    this.chunkSize = chunkSize;
  }


  /**
   * @author Nil Panchal Jun 7, 2022 9:35:40 PM
   * @param text
   */
  public SMSRequestPayload(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getFrom() {
    return from;
  }

  public String getTo() {
    return to;
  }


  public void setFrom(String from) {
    this.from = from;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public double getChunkSize() {
    return chunkSize;
  }

  public void setChunkSize(double chunkSize) {
    this.chunkSize = chunkSize;
  }



}
