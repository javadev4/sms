/**
 * @author Nil Panchal Mex IT Jun 7, 2022 9:40:43 PM ApiResponse.java
 *         com.springdemo.payload.response Sms
 */
package com.springdemo.payload.response;

import java.util.List;
import com.springdemo.payload.request.SMSRequestPayload;;

/**
 * @author Nil Panchal Mex IT Jun 7, 2022 9:40:43 PM
 */
public class ApiResponse {

  private int code;
  private boolean hasResult;
  private String message;
  private double totalChuckSize;
  private List<String> spilltedTexts;

  /**
   * @author Nil Panchal Jun 7, 2022 9:42:04 PM
   */
  public ApiResponse() {
    super();
  }

  /**
   * @author Nil Panchal Jun 7, 2022 9:42:05 PM
   * @param code
   * @param hasResult
   * @param message
   */
  public ApiResponse(int code, boolean hasResult, String message, List<String> spilltedTexts,
      double totalChuckSize) {
    this.code = code;
    this.hasResult = hasResult;
    this.message = message;
    this.spilltedTexts = spilltedTexts;
    this.totalChuckSize = totalChuckSize;
  }

  public int getCode() {
    return code;
  }

  public boolean isHasResult() {
    return hasResult;
  }

  public String getMessage() {
    return message;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public void setHasResult(boolean hasResult) {
    this.hasResult = hasResult;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public List<String> getSpilltedTexts() {
    return spilltedTexts;
  }

  public void setSpilltedTexts(List<String> spilltedTexts) {
    this.spilltedTexts = spilltedTexts;
  }

  public double getTotalChuckSize() {
    return totalChuckSize;
  }

  public void setTotalChuckSize(double totalChuckSize) {
    this.totalChuckSize = totalChuckSize;
  }



}
