package com.springdemo;


import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.nio.charset.Charset;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.springdemo.constants.Constants;
import com.springdemo.payload.request.SMSRequestPayload;

@SpringBootTest(classes = SmsApplication.class)
@TestInstance(Lifecycle.PER_CLASS)
class SmsApplicationTests {
  protected MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
      MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

  protected MockMvc mockMvc;
  protected ObjectMapper mapper;
  @Autowired
  protected WebApplicationContext webApplicationContext;

  @BeforeAll
  public void setup() throws Exception {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    this.mapper = new ObjectMapper();
  }

  @Test
  @DisplayName("Testing if text is more than 160 characters then it should spilt text into parts")
  void splitLongTextInToChuncks() throws Exception {
    SMSRequestPayload request = getLongRequestMock();
    ResultActions result = mockMvc.perform(post(Constants.SEND_SMS)
        .content(mapper.writeValueAsString(request)).contentType(contentType));
    result.andDo(print());
    result.andExpect(status().isOk()).andExpect(jsonPath("$.spilltedTexts").isArray())
        .andExpect(jsonPath("$.spilltedTexts").isNotEmpty())
        .andExpect(jsonPath("$.spilltedTexts.length()", is(4)));
  }

  @Test
  @DisplayName("Testing if text is less than 160 characters then it should not split text into parts")
  void doNotDivideSmallText() throws Exception {
    SMSRequestPayload request = getSmallRequestMock();
    ResultActions result = mockMvc.perform(post(Constants.SEND_SMS)
        .content(mapper.writeValueAsString(request)).contentType(contentType));
    result.andDo(print());
    result.andExpect(status().isOk()).andExpect(jsonPath("$.spilltedTexts").isArray())
        .andExpect(jsonPath("$.spilltedTexts").isNotEmpty())
        .andExpect(jsonPath("$.spilltedTexts.length()", is(1)));
  }

  private SMSRequestPayload getLongRequestMock() {
    return new SMSRequestPayload(
        "Each SMS can only be a maximum of 160 characters. If the user wants to send a message bigger than that, we need to break it up. We want a multi-part message to have this suffix added to each message. This method actually sends the message via an already existing SMS carrier. The solution should comply with common software standards, be well documented and supported by tests. Regarding the development environment etc we have no restrictions or requirements.",
        "Nil", "Jack", 160.0);
  }

  private SMSRequestPayload getSmallRequestMock() {
    return new SMSRequestPayload("Each SMS can only be a maximum of 160 characters.", "Nil", "Jack",
        160.0);
  }

}
