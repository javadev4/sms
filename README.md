# Sms

## Getting started

This is project to send sms.

Features
- Send SMS
- Split the text into parts if the text is more than message length

API to send sms

```
URL : http://localhost:9001/sendsms
Method : POST
```

Request Body

```
{
    "text": "Each SMS can only be a maximum of 160 characters. If the user wants to send a message bigger than that, we need to break it up. We want a multi-part message to have this suffix added to each message. This method actually sends the message via an already existing SMS carrier. The solution should comply with common software standards, be well documented and supported by tests. Regarding the development environment etc we have no restrictions or requirements.",
    "chunkSize": 160,
    "from": "Nil",
    "to": "Jack"
}
```